from setuptools import setup, find_packages
from setuptools.command.install import install
import subprocess

class PostInstallCommand(install):
    """Post-installation for installation mode."""
    def run(self):
        install.run(self)
        subprocess.call(['python', '-m', 'spacy', 'download', 'en_core_web_sm'])

setup(
    name='torchic_tab_heuristic',
    version='0.1.0',
    packages=find_packages(),
    conda_deps=[],  # Path to your exported YAML file
    cmdclass={
        'install': PostInstallCommand,
    },
)